import 'package:flutter/material.dart';

class Answer extends StatelessWidget {
  final VoidCallback selectedAnswer;
  final String answerText;
  final ButtonStyle style = ElevatedButton.styleFrom(
      textStyle: const TextStyle(fontSize: 20, color: Colors.blueAccent));

  Answer(this.selectedAnswer, this.answerText);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: ElevatedButton(
        style: style,
        onPressed: selectedAnswer,
        child: Text(answerText),
      ),
    );
  }
}
