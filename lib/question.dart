import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

class Question extends StatelessWidget {
  final String questionText;
  //if we don't add final to questionText that mean we can change  value of it
  //if we add final then it not gonna never change for the Question widget

  Question(this.questionText);
  //this gonna assign line number 6 value (questionText,property) from main dart 39 line
  //this taking value of question from main file as in constructor

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.all(10),
      child: Text(
        questionText,
        style: TextStyle(fontSize: 28),
        textAlign: TextAlign.center,
      ),
    );
  }
}
