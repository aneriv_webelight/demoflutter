import 'package:first_flutter/quiz.dart';
import 'package:first_flutter/result.dart';
import 'package:flutter/material.dart';

void main() => runApp(DemoApp());

class DemoApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _DemoAppState();
  }
}

class _DemoAppState extends State<DemoApp> {
  final _questions = const [
    {
      "question": "What's your name?",
      "answer": [
        {"text": "One", "mark": 10},
        {"text": "Two", "mark": 5},
        {"text": "Three", "mark": 2},
        {"text": "Four", "mark": 4}
      ]
    },
    {
      "question": "How are you?",
      "answer": [
        {"text": "Billli", "mark": 10},
        {"text": "Aneri", "mark": 5},
        {"text": "Divy", "mark": 2},
        {"text": "Yoo", "mark": 4}
      ]
    },
    {
      "question": "Fav Animal?",
      "answer": [
        {"text": "yyooo", "mark": 10},
        {"text": "ssss", "mark": 5},
        {"text": "wwww", "mark": 2},
        {"text": "ddsds", "mark": 4}
      ]
    },
  ];
  var _questionIndex = 0;
  var _totalMarks = 0;
  //_define it's private property
  void _addAnswer(int mark) {
    _totalMarks += mark;
    setState(() {
      _questionIndex = _questionIndex + 2;
    });
    // print(_questions[_questionIndex]);
    if (_questionIndex < _questions.length) {
      print(_questionIndex);
    } else {
      print('No more question found');
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
            appBar: AppBar(
              title: Text('Hello My First App'),
            ),
            body: _questionIndex < _questions.length
                ? Quiz(
                    questions: _questions,
                    questionIndex: _questionIndex,
                    selectedAnswer: _addAnswer)
                : Result(_totalMarks)));
  }
}
