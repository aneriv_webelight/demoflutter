import 'dart:ffi';

import 'package:flutter/material.dart';
import './question.dart';
import './answer.dart';

class Quiz extends StatelessWidget {
  final List<Map<String, Object>> questions;
  final int questionIndex;
  final Function selectedAnswer;
  Quiz(
      {required this.questions,
      required this.questionIndex,
      required this.selectedAnswer});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Question(questions[questionIndex]['question'] as String),
        //above we pass dynamic question to common question component
        ///... will have one list with nested one
        ...(questions[questionIndex]['answer'] as List<Map<String, Object>>)
            .map((answer) {
          return Answer(
              () => selectedAnswer(answer['mark']), answer['text'] as String);
        }).toList()
      ],
    );
  }
}
