import 'package:flutter/material.dart';

class Result extends StatelessWidget {
  final int finalMark;
  Result(this.finalMark);

  String get updatedResult {
    var resultText = 'Questions are over buddy!';
    if (finalMark <= 8) {
      resultText = ' you got ${finalMark}';
      return resultText;
    } else if (finalMark > 3) {
      resultText = 'Questions are over buddy!${finalMark}';
      return resultText;
    } else {
      return resultText;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        updatedResult,
        style: TextStyle(fontSize: 36, fontWeight: FontWeight.bold),
      ),
    );
  }
}
